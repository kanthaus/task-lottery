let uniqueId = 0

export const getEmptySlot = () => ({ name: `_empty_${uniqueId++}`, empty: true })

export const getTask = taskLabel => ({ label: taskLabel, slots: [ getEmptySlot(), getEmptySlot() ] })

export function assignVolunteers ({ participants, volunteers, tasks, assignVolunteer }) {
  Object.keys(volunteers).forEach(taskLabel => {
    const taskVolunteers = volunteers[taskLabel]
    const task = tasks.find(t => t.label === taskLabel)
    taskVolunteers.forEach(name => {
      const participant = participants.find(p => p.name.toLowerCase() === name.toLowerCase())
      assignVolunteer(task, participant)
    })
  })
}

export const notAssigned = participant => participant.assigned == null
export const notVolunteered = participant => !participant.volunteered

export const hasAvailableSlots = task => task.slots.filter(slot => slot.empty).length > 0
