import { uniq, flatten } from './utils'

const lotteryHeaderPattern = /#{2,6}.{1,20}lottery/i
const headerPattern = /^\s*#* /
const taskLinePattern = /\s?[*-]\s?(.*):(.*)/
const presentLineStart = /[-*] Present:/
const neverAssigned = new Set([ 'mika', 'levi' ])

export default async () => {
  const text = await fetch('https://pad.kanthaus.online/come/download', {
    mode: 'cors',
  })
  .then(res => res.text())
  return parse(text)
}

const parse = text => {
  const data = {
    participants: [],
    tasksNames: [],
    volunteers: {},
    unavailabilities: {},
  }

  if (text.split(presentLineStart)[1]) {
    const present = text.split(presentLineStart)[1].split('\n')[0].trim()
    const presentParticipantsNames = present !== '' ? present.split(/,\s?/) : []
    presentParticipantsNames.forEach(name => addIfMissing(data.participants, name))
  }

  if (text.split(lotteryHeaderPattern)[1]) {
    const lines = text
      .split(lotteryHeaderPattern)[1]
      .split('\n')

    let section

    for (let line of lines) {
      line = removeBoldMarkup(line)
      if (line.match(headerPattern)) {
        section = findSection(line)
      } else if (section != null && data[section] != null) {
        const [ , taskLabel, participantsNames ] = (line.match(taskLinePattern) || [])
        const isTaskName = taskLabel && !taskLabel.includes('~~')
        if (isTaskName && participantsNames && participantsNames.trim().length > 0) {
          const names = participantsNames
            .trim()
            .split(/\s*[,+]\s*/)
            .filter(isntEmpty)
            .map(toLowerCase)
          // Add any non-present volunteer to the list of participants
          names.forEach(name => addIfMissing(data.participants, name))
          data[section][taskLabel] = names
        }
        if (section === 'volunteers' && isTaskName && taskLabel !== 'Week') data.tasksNames.push(taskLabel)
      }
    }
  }

  if (data.unavailabilities.Week) {
    const normalizedWeekUnavailabilitiesNames = data.unavailabilities.Week.map(toLowerCase)
    const normalizedVolunteersNames = flatten(Object.values(data.volunteers)).map(toLowerCase)
    data.participants = data.participants.filter(name => {
      const normalizedName = name.toLowerCase()
      return !(normalizedWeekUnavailabilitiesNames.includes(normalizedName) && !normalizedVolunteersNames.includes(normalizedName))
    })
  }

  data.participants = uniq(data.participants)
    .filter(canBeAssigned)
    .map(name => ({
      name,
      unavailabilities: findUnavailabilities(data.unavailabilities, name)
    }))

  delete data.unavailabilities.Week

  console.log('imported data', data)

  return data
}

const removeBoldMarkup = line => line.replace(/\*\*/g, '')

const findSection = headerLine => {
  if (headerLine.toLowerCase().includes('vol')) return 'volunteers'
  else if (headerLine.toLowerCase().includes('unavail')) return 'unavailabilities'
  else return null
}

const findUnavailabilities = (unavailabilities, name) => {
  const participantUnavailabilities = []
  Object.keys(unavailabilities).forEach(taskLabel => {
    const taskUnavailabilities = unavailabilities[taskLabel]
    if (taskUnavailabilities && taskUnavailabilities.includes(name)) {
      participantUnavailabilities.push(taskLabel)
    }
  })
  return participantUnavailabilities
}

const canBeAssigned = name => !(neverAssigned.has(name.trim().toLowerCase()))

const addIfMissing = (participantsNames, name) => {
  if (name.trim().length === 0) return
  const isAlreadyAdded = participantsNames.map(toLowerCase).includes(name.toLowerCase())
  if (!isAlreadyAdded) participantsNames.push(name)
}

const toLowerCase = str => str.toLowerCase()

const isntEmpty = str => str != null && str.length > 0
