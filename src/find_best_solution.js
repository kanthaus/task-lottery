import { notAssigned, notVolunteered } from './helpers'
import { shuffleArray, sum, byIndex } from './utils'

export default function ({ tasks, participants, unavailabilities }) {
  const emptySlots = getEmptySlots(tasks)

  participants = participants.filter(notVolunteered)

  const maxScore = emptySlots.length > participants.length ? participants.length - emptySlots.length : 0

  console.log({ emptySlots, participants, unavailabilities, maxScore })

  return generateSolution({ emptySlots, participants, unavailabilities, maxScore, attempts: 0, totalAttempts: 0 })
}

const getEmptySlots = tasks => {
  const slots = []
  tasks.forEach(task => {
    task.slots.forEach((slot, index) => {
      if (slot.empty) slots.push({ task, index })
    })
  })
  return slots
}

const generateSolution = ({ emptySlots, participants, unavailabilities, maxScore, attempts, totalAttempts, bestSolution }) => {
  const shuffledEmptySlots = shuffleArray(emptySlots)
    // Fill first task slots first
    .sort(byIndex)

  const shuffledParticipants = shuffleArray(participants)

  shuffledParticipants.forEach(participant => {
    const remainingSlots = shuffledEmptySlots.filter(notAssigned)
    if (remainingSlots.length > 0) {
      const slot = remainingSlots.find(slot => {
        const taskLabel = slot.task.label
        const taskUnavailabilities = unavailabilities[taskLabel] || []
        return slot.assigned == null && !taskUnavailabilities.includes(participant.name.toLowerCase())
      })
      if (slot) slot.assigned = participant
    }
  })

  const missingPerTask = {}

  shuffledEmptySlots
  .filter(notAssigned)
  .forEach(slot => {
    missingPerTask[slot.task.label] = missingPerTask[slot.task.label] || 0
    missingPerTask[slot.task.label]++
  })

  let score = 0 - sum(Object.values(missingPerTask).map((num = 0) => num ** 2))

  if (missingPerTask['Open Tuesday'] === 2) {
    score -= 1000
  }

  ++totalAttempts
  ++attempts

  const solution = { assignements: shuffledEmptySlots, score, totalAttempts }
  if (bestSolution == null || solution.score > bestSolution.score) bestSolution = solution

  if (score >= maxScore) {
    return solution
  } else {
    if (totalAttempts > 1000) {
      console.error('too many attempts')
      return bestSolution
    }
    if (attempts > 20) {
      maxScore--
      attempts = 0
    }

    // Cleanup to reuse the same objects
    emptySlots.forEach(slot => delete slot.assigned)

    return generateSolution({
      emptySlots,
      participants,
      unavailabilities,
      maxScore,
      attempts,
      totalAttempts,
      bestSolution
    })
  }
}
