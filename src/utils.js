export const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

export function shuffleArray (array) {
  return array
  .map(el => ({ el, score: Math.random() }))
  .sort((a, b) => a.score - b.score)
  .map(({ el }) => el)
}

export function sum (array) {
  let total = 0
  array.forEach(num => { total += num })
  return total
}

export const byIndex = (a, b) => a.index - b.index

export const noop = () => {}

export const uniq = array => Array.from(new Set(array))

export const flatten = array => [].concat(...array)
