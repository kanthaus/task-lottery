# Task Lottery

A web UI to randomly assign tasks, typically dinner preparation, to a list of people. It's coupled to the [Kanthaus](https://kanthaus.online/) use-case, but could easily be adapted to other use-cases, by changing the way default options are determined (see `src/import_data_from_pad.js`).

[Demo](https://kanthaus.gitlab.io/task-lottery/)

Svelte rewrite of the [Dinner Lottery](https://gitlab.com/kanthaus/dinner-lottery) tool.

## LICENSE
[AGPL-3](http://www.gnu.org/licenses/agpl-3.0.html)
