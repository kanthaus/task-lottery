#!/usr/bin/env bash
set -eu

git switch main
git push origin main
git switch --force-create dist
npm run build
git add --force ./public/build
git commit --message 'updated build'
git push origin dist --force
git switch main
